<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header'); ?>

  <section class="">
    <div class="wrap hpad">

      <article itemscope itemtype="http://schema.org/BlogPosting">

        <header>
          <h1 itemprop="headline">
            <?php the_title(); ?>
          </h1>
        </header>

        <div itemprop="articleBody">
          <?php the_content(); ?>
        </div>

      </article>

    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>