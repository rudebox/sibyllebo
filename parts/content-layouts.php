<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'slider' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'link-boxes' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'link-boxes' ); ?>

    <?php
    } elseif( get_row_layout() === 'latest-appartments' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'latest-appartments' ); ?>

    <?php
    } elseif( get_row_layout() === 'accordions' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'accordions' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
