 <?php 
//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$text = get_sub_field('text');

$link = get_sub_field('link');
$link_text = get_sub_field('link_text');

$center = get_sub_field('center');

if ($center === true) {
	$center = 'center';
} else {
	$center = '';
}

//query arguments
$args = array(
  'posts_per_page' => 3,
  'post_type' => 'bolig',
  'orderby' => 'date'
);
 
$query = new WP_QUERY($args);
           
?>

<?php if ($query->have_posts() ) : ?>

 <section class="appartments padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>">

    <div class="wrap hpad">

      <div class="appartments__intro <?php echo esc_attr($center); ?>">
	      <h2 class="appartments__header"><?php echo esc_html($title); ?></h2>
	      <?php echo $text; ?>
      </div>

      <div class="row flex flex--wrap">
      
            <?php 

               while ($query->have_posts() ) : $query->the_post();
              //get thumb
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
              //post img alt tag
              $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true); 

              $squaremeter = get_field('squaremeter');
              $rooms = get_field('rooms');
              $period = get_field('rent_period');
              $date = get_field('rent_date');
              $rent = get_field('rent');

            ?>

             <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="appartments__item col-sm-6 col-md-4 anim fade-up" itemscope itemtype="http://schema.org/Product">

                <header>
                  <div itemprop="image" class="appartments__img" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">
                    
                  </div>
                </header>

                <div class="appartments__content" itemprop="description">
                	<h2 class="appartments__title h5" itemprop="name">                   
                      <?php the_title(); ?>
                  	</h2>
                  	<div class="flex flex--justify flex--wrap appartments__details">
            			<span><span class="blue-medium">Kvm</span>	<?php echo esc_html($squaremeter); ?></span> 
            			<span><span class="blue-medium">Lejeperiode</span> <?php echo esc_html($period); ?></span>
            		</div>
            		<div class="flex flex--justify flex--wrap appartments__details">
            			<span><span class="blue-medium">Rum</span> <?php echo esc_html($rooms); ?></span>            
            			<span><span class="blue-medium">Overtagelse</span> <?php echo esc_html($date); ?></span>
            		</div>
            		<div class="flex flex--justify flex--wrap appartments__details">
            			<span>Månedlig leje</span><strong><?php echo esc_html($rent); ?>,-</strong>
            		</div>
                </div>

              </a>

            <?php endwhile; wp_reset_postdata();  ?>

            <?php else: ?>

            	<?php get_template_part('parts/content', 'none'); ?>
            <?php endif; ?>

      </div>

      <?php if ($link) : ?>
      	<div class="center appartments__btn">
      		<a class="btn btn--blue" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a> 
      	</div>
  	  <?php endif; ?>

    </div>
  </section>