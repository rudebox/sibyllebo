<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$text = get_sub_field('text');

$center = get_sub_field('center');

if ($center === true) {
	$center = 'center';
} else {
	$center = '';
}

if (have_rows('linkbox') ) :
?>

<section class="link-boxes <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">

		<div class="link-boxes__intro <?php echo esc_attr($center); ?>">
			<h2 class="link-boxes__header"><?php echo esc_html($title); ?></h2>
			<?php echo $text; ?>
		</div>

		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');
			?>

			<div class="col-sm-4 link-boxes__item center anim fade-up"> 
				<?php if ($icon) : ?>
					<div class="link-boxes__icon">
						<img class="link-boxes__icon--img" src="<?php echo esc_url($icon['url']); ?>" alt="icon">
					</div>
				<?php endif; ?>
				<h2 class="h6"><?php echo esc_html($title); ?></h2>
				<?php echo $text; ?>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>