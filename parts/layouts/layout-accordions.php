<?php 
/**
* Description: Lionlab accordions repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$text = get_sub_field('text');

$center = get_sub_field('center');

if ($center === true) {
	$center = 'center';
} else {
	$center = '';
}

if (have_rows('accordion') ) :
?>

<section class="accordion bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">

		<div class="accordion__intro <?php echo esc_attr($center); ?>">
			<h2 class="accordion__header"><?php echo esc_html($title); ?></h2>
			<?php echo $text; ?>
		</div>

		<div class="row flex flex--wrap">
			<?php while (have_rows('accordion') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
			?>

			<div class="col-sm-8 col-sm-offset-2 accordion__item anim fade-up">
				<div class="accordion__wrapper">
					<h3 class="accordion__title"><?php echo esc_html($title); ?> <i class="fas fa-angle-down"></i></h3>
					<div class="accordion__panel">
						<?php echo $text; ?>
					</div>
				</div>	
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>