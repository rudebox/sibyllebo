<?php
    /**
    * Google Maps
    * @since 2.4.0
    **/

    //query arguments
    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'Bolig'
    );

    $archive_title = get_field('appartment_archive_title', 'options');
     
    $query = new WP_QUERY($args);

    if ( $query->have_posts() ) : 

    ?>

    <section class="appartments padding--top">
        <div class="wrap hpad">

            <h2><?php echo esc_html($archive_title); ?></h2>

            <?php 
                //get top level categories for filtering thorugh query
                $categories = get_categories( array(
                    'orderby' => 'name',
                    'parent'  => 0
                ) );
            ?>

            <div class="appartments__row">

                <div class="col-sm-4 col-md-2 search-result__categories">
            
                  <form class="search-result__filter">
                      
                      <p><?php _e('Kategorier', 'lionlab'); ?></p>  

                      <label for="Alle">
                        <input class="search-result__checkbox search-result__checkbox--all" type="radio" checked="checked" id="all" name="filter" value="all" onchange="filterMarker(this.value);"></input>
                        <?php _e('Alle', 'lionlab'); ?>
                      </label>

                      <?php foreach ($categories as $category) : ?>
                        <label for="<?php echo $category->name; ?>">
                            <input class="search-result__checkbox" type="radio" id="<?php echo $category->name; ?>" name="filter" value="<?php echo $category->name; ?>" onchange="filterMarker(this.value);"></input>
                            <?php echo $category->name; ?>
                        </label>
                      <?php endforeach; ?>
                  </form>

                </div>

                <div class="google-map js-maps col-sm-8 col-md-10">

                    <?php
                    // Loop Google Maps
                    while ( $query->have_posts() ) : $query->the_post();
                        $location = get_field('location');
                        $address = get_field('address') ?: $location['address'];

                        //post img
                        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );

                        //post img alt tag
                        $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);  

                         //get primary category - see function in lionlab-helpers.php
                        $post_categories = get_post_primary_category($post->ID, 'category');

                        $primary_category = $post_categories['primary_category']->name;
                     ?>
        
                    <?php if ($location) : ?>
                        <div class="marker google-map__marker" data-category="<?php echo $primary_category; ?>"  data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                            
                            <?php if ($thumb) : ?>
                            <a href="<?php the_permalink(); ?>" class="google-map__thumb" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);"></a>
                            <?php endif; ?>
                            
                            <a href="<?php the_permalink(); ?>">
                                <h6 class="google-map__title"><?php the_title(); ?></h6>
                            </a>

                        </div>
                    <?php endif; ?>

                    <?php endwhile; wp_reset_postdata();  ?>

                </div>
            </div>
        </div>
    </section>    
<?php endif; ?>