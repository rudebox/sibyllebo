<?php

/*
 * Template Name: Contact
 */

get_template_part('parts/header'); the_post(); ?>

<?php 
	//contact fields
	$title = get_field('contact_title'); 
	$text = get_field('contact_text'); 
	$adress = get_field('address', 'options'); 
  	$phone = get_field('phone', 'options'); 
  	$mail = get_field('mail', 'options'); 
?>

<main>

	<?php get_template_part('parts/page', 'header');?>

	<section class="contact padding--both">
		<div class="wrap hpad">
			<div class="row flex flex--wrap">

				<div class="col-sm-6 contact__info">
					<h3 class="contact__title"><?php echo esc_html($title); ?></h3>
					<?php echo $text; ?>
				</div>

				<div class="col-sm-6 contact__form">
					<?php gravity_form( 1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
				</div>
			</div>
		</div>
	</section>


</main>

<?php get_template_part('parts/footer'); ?>
