<?php
/**
 * Description: Lionlab sliders
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

if ( have_rows('slides') ) : ?>

  <section class="slider padding--bottom">
    <div class="slider__track is-slider">

      <?php
      // Loop through slides
      while ( have_rows('slides') ) :
        the_row();
        $image = get_sub_field('slides_bg'); 
      ?>

        <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
        </div>

      <?php endwhile; ?>

    </div>

    <div class="slider__thumbs flex flex--wrap flex--valign"> 

    <?php
      // Loop through slides
      while ( have_rows('slides') ) :
        the_row();
        $image = get_sub_field('slides_bg'); 
        $index = get_row_index();
      ?>

        <div data-dot="<?php echo esc_attr($index); ?>" class="slider__thumb" style="background-image: url(<?php echo esc_url($image['url']); ?>);"></div> 

      <?php endwhile; ?>
    </div>
  </section>
<?php endif; ?>