<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	$page_img = get_field('page_img') ? : $page_img = get_field('page_img', 'options');

	$archive_page_img = get_field('appartment_page_img', 'options');
?>

<?php if (is_front_page() ) : ?>
<section class="page__hero page__hero--frontpage" style="background-image: url(<?php echo esc_url($page_img['url']); ?>);">
<?php elseif (is_post_type_archive('bolig') ) : ?>
<section class="page__hero" style="background-image: url(<?php echo esc_url($archive_page_img['url']); ?>);">
<?php else : ?>
<section class="page__hero" style="background-image: url(<?php echo esc_url($page_img['url']); ?>);">
<?php endif; ?>
	<div class="wrap hpad center page__container">
		<h1 class="page__title"><?php echo $title; ?></h1>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/divider.svg" alt="divider">
	</div>
</section>