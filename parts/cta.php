<?php 
//fields

$bg = get_field('cta_bg', 'options');
$text = get_field('cta_text', 'options');
$title = get_field('cta_title', 'options');
$link = get_field('cta_link', 'options');
$link_text = get_field('cta_link_text', 'options');
?>

<section class="cta" style="background-image: url(<?php echo esc_url($bg['url']); ?>);">
	<div class="wrap hpad">
		<div class="row">

			<div class="col-sm-8 col-sm-offset-2 cta__text center">
				<h2 class="cta__title"><?php echo esc_html($title); ?></h2>
				<?php echo $text; ?>

				<a class="btn btn--white cta__btn" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
			</div>

		</div>
	</div>
</section>