<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

<div class="transition__pace"></div>

<div id="transition" class="transition">

<div class="transition__container" data-namespace="general"> 

<header class="header" itemscope itemtype="http://schema.org/WPHeader">
  <div class="wrap hpad flex flex--center flex--justify">

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_ny.png" alt="<?php bloginfo('name'); ?>">
    </a>

    <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <div class="nav--mobile">
        <?php lionlab_main_nav(); ?>
        <?php if ( has_nav_menu( 'lionlab-cta-nav' ) ) : ?>
        <div class="hidden-desktop">
          <?php lionlab_cta_nav(); ?>
        </div>
      <?php endif; ?>
      </div>
    </nav>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

    <?php if ( has_nav_menu( 'lionlab-cta-nav' ) ) : ?>
    <div class="hidden-mobile">
      <?php lionlab_cta_nav(); ?>
    </div>
    <?php endif; ?>

  </div>
</header>


