<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php 
    get_template_part('parts/page', 'header'); 

    $squaremeter = get_field('squaremeter');
    $rooms = get_field('rooms');
    $period = get_field('rent_period');
    $date = get_field('rent_date');
    $rent = get_field('rent');
    $available = get_field('apparment_available');
  ?>

  <section class="appartment padding--both">
    <div class="wrap hpad">

      <div class="row">

        <article class="col-sm-12" itemscope itemtype="http://schema.org/BlogPosting">

          <header class="appartments__header center">
            <h2 itemprop="headline">
              <?php the_title(); ?>
            </h2>
            <h4 class="blue"><?php echo esc_html($rent); ?>,- pr. måned</h4>
            <div class="flex flex--justify flex--wrap appartments__details">
              <span class="appartments__details-detail"><i class="fas fa-ruler-combined"></i> <span class="blue-medium">Kvm</span> <?php echo esc_html($squaremeter); ?></span> 
              <span class="appartments__details-detail"><i class="fas fa-clock"></i> <span class="blue-medium">Lejeperiode</span> <?php echo esc_html($period); ?></span>                       
              <span class="appartments__details-detail"><i class="fas fa-door-open"></i> <span class="blue-medium">Rum</span> <?php echo esc_html($rooms); ?></span>            
              <span class="appartments__details-detail"><i class="fas fa-key"></i> <span class="blue-medium">Overtagelse</span> <?php echo esc_html($date); ?></span>                      
            </div>
          </header>

          <?php get_template_part('parts/gallery'); ?>

          <div class="col-sm-8 col-sm-offset-2" itemprop="articleBody">
            <?php the_content(); ?>

            <?php if ($available === true) : ?>
              <div class="col-sm-12 center padding--top">
                <a class="btn btn--blue" href="mailto:info@sibyllebo.dk?subject=Jeg ønsker at ansøge lejemålet <?php echo the_title(); ?>&body=Mine kontaktoplysninger er som følgende:">Ansøg lejemål</a>
              </div>
            <?php endif; ?>
          </div>

        </article>

      </div>
    </div>
  </section>

  <?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>