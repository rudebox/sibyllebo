<?php get_template_part('parts/header'); ?>

<main>

  <section class="error">

  	<div class="wrap hpad">

	    <h1>Siden kunne ikke findes</h1>
	    <p>Beklager, men siden findes ikke.</p>

    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>