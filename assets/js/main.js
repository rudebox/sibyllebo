jQuery(document).ready(function($) {

    /*
     *  render_map
     *
     *  This function will render a Google Map onto the selected jQuery element
     *
     *  @type  function
     *  @date  8/11/2013
     *  @since 4.3.0
     *
     *  @param $el (jQuery element)
     *  @return  n/a
     */

    function new_map($el) {

      // var
      var $markers = $el.find('.marker');

      // vars
      var args = {
        zoom: 16,
        center: new google.maps.LatLng(0, 0),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles      :  [    {
                            "featureType": "transit",
                            "stylers": [ 
                              { "visibility": "off" }
                            ]
                          },{
                            "featureType": "poi",
                            "stylers": [
                              { "visibility": "off" }
                            ]
                          },{
                            "featureType": "landscape",
                            "stylers": [
                              { "visibility": "off" }
                            ]
                          }],
      };

      // create map           
      var map = new google.maps.Map($el[0], args);

      // add a markers reference
      map.markers = [];

      // add markers
      $markers.each(function() {

        add_marker($(this), map);

      });

      // center map
      center_map(map);

    }

    /*
     *  add_marker
     *
     *  This function will add a marker to the selected Google Map
     *
     *  @type  function
     *  @date  8/11/2013
     *  @since 4.3.0
     *
     *  @param $marker (jQuery element)
     *  @param map (Google Map object)
     *  @return  n/a
     */

    function add_marker($marker, map) {

      // var
      var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

      //category
      var category = $marker.attr('data-category');

      // create marker
      var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: '/wp-content/uploads/2020/11/map-marker.png',
        category    : category
      });

      // add to array
      map.markers.push(marker);

      // if marker contains HTML, add it to an infoWindow
      if ($marker.html()) {
        // create info window
        var infowindow = new google.maps.InfoWindow({
          content: $marker.html()
        });

        // show info window when marker is clicked
        google.maps.event.addListener(marker, 'click', function() {

          infowindow.open(map, marker);

        });
      }

          //filter through markers based on categories 

    filterMarker = function (id) {

      for (i = 0; i < map.markers.length; i++) {
        marker = map.markers[i];

        if (document.getElementById('all').checked) {                          
            marker.setVisible(true);
        } else {
            marker.setVisible(false);
        } 

        if (document.getElementById(id).checked) {
          if (marker.category == id) {
            marker.setVisible(true);
          } 
        } 

        else {
          if (marker.category == id) {
            marker.setVisible(false);
          }
        }
      } 
    }

    }


    /*
     *  center_map
     *
     *  This function will center the map, showing all markers attached to this map
     *
     *  @type  function
     *  @date  8/11/2013
     *  @since 4.3.0
     *
     *  @param map (Google Map object)
     *  @return  n/a
     */

    function center_map(map) {

      // vars
      var bounds = new google.maps.LatLngBounds();

      // loop through all markers and create bounds
      $.each(map.markers, function(i, marker) {

        var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

        bounds.extend(latlng);

      });

      // only 1 marker?
      if (map.markers.length == 1) {
        // set center of map
        map.setCenter(bounds.getCenter());
        map.setZoom(16);
      } else {
        // fit to bounds
        map.fitBounds(bounds);
      }

    }

    /*
     *  document ready
     *
     *  This function will render each map when the document is ready (page has loaded)
     *
     *  @type  function
     *  @date  8/11/2013
     *  @since 5.0.0
     *
     *  @param n/a
     *  @return  n/a
     */



    $('.js-maps').each(function() {

      map = new_map($(this));

    });


  function init() {

    //in viewport check
    var $animation_elements = $('.anim');
    var $window = $(window);

    function check_if_in_view() {
      var window_height = $window.height();
      var window_top_position = $window.scrollTop();
      var window_bottom_position = (window_top_position + window_height);
     
      $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);
     
        //check to see if this current container is within viewport
        if ((element_top_position <= window_bottom_position)) {
          $element.addClass('in-view');
        } else {
          $element.removeClass('in-view');
        }
      }); 
    }

    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');


    //menu toggle
    $('.nav-toggle').click(function(e) {
      e.preventDefault();
      $('.nav--mobile').toggleClass('is-open');
      $('body').toggleClass('is-fixed');
    });
    

    //fancybox
    $('[data-fancybox]').fancybox({ 
      toolbar  : false,
      smallBtn : true,
      iframe : { 
        preload : false 
      }
    });


  //toggle search categories - match up input values with search results categories
  var $input = $('.search-result__checkbox');

  if ($('.search-result__checkbox--all').prop('checked') === true) {
      $('.search-result__item').addClass('is-visible');
  } 

  $input.on('change', function() {
    var data = $(this).val();
    var $item = $('.search-result__item');

    $item.each(function() {
      if ($(this).attr('data-category') === data) { 
        $(this).addClass('is-visible');
      } else {
        $(this).removeClass('is-visible');  
      }

      if ($('.search-result__checkbox--all').prop('checked') === true) {
        $('.search-result__item').addClass('is-visible');
      } 
    });
  });


    //owl slider/carousel
    var owl = $('.slider__track');

    owl.each(function() {
    $(this).children().length > 1;

      owl.owlCarousel({
          loop: true,
          items: 1,
          autoplay: true,
          mouseDrag: false,
          // nav: true,
          dots: true,
          dotsContainer: '.slider__thumbs', 
          dotsData: true,
          autplaySpeed: 11000,
          autoplayTimeout: 10000,
          smartSpeed: 250,
          smartSpeed: 2200,
          navSpeed: 2200
          // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
      });

    });

    $('.slider__thumb').click(function() {
      owl.trigger('to.owl.carousel', [$(this).index(), 1000]);
    });


    //toggle accordion  
    var $acc = $('.accordion__title'); 

      $acc.each(function() {
        $(this).click(function() {
          $(this).toggleClass('is-active');
          $(this).next().toggleClass('is-visible');
      }); 
    });

  }

  init();

 //
  // Barba.js
  //
  Barba.Pjax.Dom.wrapperId = 'transition';
  Barba.Pjax.Dom.containerClass = 'transition__container';
  Barba.Pjax.ignoreClassLink = 'no-ajax';

  var general = Barba.BaseView.extend({
    namespace: 'general',
    onEnter: function() {
      // The new Container is ready and attached to the DOM.
    
    },
    onEnterCompleted: function() {
      // The Transition has just finished.
      $('.transition__pace').removeClass('is-active');
      $('html').removeClass('is-loading'); 
    },
    onLeave: function() {
      // A new Transition toward a new page has just started.
      $('.transition__pace').addClass('is-active');
      $('html').addClass('is-loading'); 
    },
    onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.
  
    }
  });


  general.init();

  Barba.Pjax.start();

  Barba.Prefetch.init(); 

  Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;

  Barba.Pjax.preventCheck = function(evt, element) {
    if (!Barba.Pjax.originalPreventCheck(evt, element)) {
      return false;
    }

    //outcomment if you want to prevent barba when logged in
    // if ($('body').hasClass('logged-in')) { Barba.Pjax.preventCheck = function() { 
    //   return false;  
    // }; } 

    // No need to check for element.href -
    // originalPreventCheck does this for us! (and more!)
    if (/.pdf/.test(element.href.toLowerCase())) {
      return false;
    } else if (/edit/.test(element.href.toLowerCase())) {
      return false;
    } else if (/wp-admin/.test(element.href.toLowerCase())) {
      return false;
    }

    return true;
  };

  var FadeTransition = Barba.BaseTransition.extend({
  start: function() {
  /**
  * This function is automatically called as soon the Transition starts
  * this.newContainerLoading is a Promise for the loading of the new container
  * (Barba.js also comes with an handy Promise polyfill!)
  */
  // As soon the loading is finished and the old page is faded out, let's fade the new page
  Promise
  .all([this.newContainerLoading, this.fadeOut()])
  .then(this.fadeIn.bind(this));
  },
  fadeOut: function() {

  /**
  * this.oldContainer is the HTMLElement of the old Container
  */
  //return $(this.oldContainer).animate({ opacity: 0 }).promise();
  return $(this.oldContainer).animate({ opacity: 1 }).promise();
  },
  fadeIn: function() {
    /**
    * this.newContainer is the HTMLElement of the new Container
    * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
    * Please note, newContainer is available just after newContainerLoading is resolved!
    */
    var _this = this;
    var $newContainer = $(this.newContainer);

    $(this.oldContainer).hide();
    $(window).scrollTop(0); // scroll to top here

    $newContainer.css({
      visibility : 'visible',
      opacity: 1
    });

    $newContainer.animate({
      opacity: 1,
    }, 400, function() {
      /**
      * Do not forget to call .done() as soon your transition is finished!
      * .done() will automatically remove from the DOM the old Container
      */
      _this.done();

    });
    }

  });
  /**
  * Next step, you have to tell Barba to use the new Transition
  */
  Barba.Pjax.getTransition = function() {
  /**
  * Here you can use your own logic!
  * For example you can use different Transition based on the current page or link...
  */
  return FadeTransition;

  };

  Barba.Dispatcher.on('initStateChange', function() {

    $('body').removeClass('is-fixed');
    $('.nav--mobile').removeClass('is-open');
    $('.nav__dropdown').removeClass('is-visible');

    init();

  });

  /**
  * GET WP body classes
  */
  Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container, newPageRawHTML) {

    // html head parser borrowed from jquery pjax
    var $newPageHead = $( '<head />' ).html(
        $.parseHTML(
            newPageRawHTML.match(/<head[^>]*>([\s\S.]*)<\/head>/i)[0]
          , document
          , true
        )
    );

    var headTags = [
        "meta[name='keywords']"
      , "meta[name='description']"
      , "meta[property^='og']"
      , "meta[name^='twitter']"
      , "meta[itemprop]"
      , "link[itemprop]"
      , "link[rel='prev']"
      , "link[rel='next']"
      , "link[rel='canonical']"
    ].join(',');
    $( 'head' ).find( headTags ).remove(); // Remove current head tags
    $newPageHead.find( headTags ).appendTo( 'head' ); // Append new tags to the head 


    //update body classes
    var response = newPageRawHTML.replace(/(<\/?)body( .+?)?>/gi, '$1notbody$2>', newPageRawHTML)
    var bodyClasses = $(response).filter('notbody').attr('class')
    $('body').attr('class', bodyClasses);


    //get current url and path
    var $path = currentStatus.url.split(window.location.origin)[1].substring(1); 
    var $url = currentStatus.url;

    //add active class based upon URL status
    $('.nav__item a').each(function() {  
      if ($url === this.href) {
        $(this).addClass('is-active');
      }

      else {
        $(this).removeClass('is-active');
        $('.nav__item').removeClass('is-active');
      }
    }); 

    $('.js-maps').each(function() {

      map = new_map($(this));

    });

    init();

  }); 

  Barba.Dispatcher.on('transitionCompleted', function (currentStatus, oldStatus, container) {

  });

});
