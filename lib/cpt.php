<?php 
// Register Custom Post Type Bolig
function create_bolig_cpt() {

	$labels = array(
		'name' => _x( 'Boliger', 'Post Type General Name', 'lionlab' ),
		'singular_name' => _x( 'Bolig', 'Post Type Singular Name', 'lionlab' ),
		'menu_name' => _x( 'Boliger', 'Admin Menu text', 'lionlab' ),
		'name_admin_bar' => _x( 'Bolig', 'Add New on Toolbar', 'lionlab' ),
		'archives' => __( 'Bolig Archives', 'lionlab' ),
		'attributes' => __( 'Bolig Attributes', 'lionlab' ),
		'parent_item_colon' => __( 'Parent Bolig:', 'lionlab' ),
		'all_items' => __( 'Alle Boliger', 'lionlab' ),
		'add_new_item' => __( 'Tilføj ny Bolig', 'lionlab' ),
		'add_new' => __( 'Tilføj ny', 'lionlab' ),
		'new_item' => __( 'Ny Bolig', 'lionlab' ),
		'edit_item' => __( 'Rediger Bolig', 'lionlab' ),
		'update_item' => __( 'Opdater Bolig', 'lionlab' ),
		'view_item' => __( 'Se Bolig', 'lionlab' ),
		'view_items' => __( 'Se Boliger', 'lionlab' ),
		'search_items' => __( 'Søg Bolig', 'lionlab' ),
		'not_found' => __( 'Intet fundet', 'lionlab' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
		'featured_image' => __( 'Udvalgt billede', 'lionlab' ),
		'set_featured_image' => __( 'Vælg udvlagt billede', 'lionlab' ),
		'remove_featured_image' => __( 'Fjern udvalgt billede', 'lionlab' ),
		'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
		'insert_into_item' => __( 'Insert into Bolig', 'lionlab' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Bolig', 'lionlab' ),
		'items_list' => __( 'Boliger list', 'lionlab' ),
		'items_list_navigation' => __( 'Boliger list navigation', 'lionlab' ),
		'filter_items_list' => __( 'Filter Boliger list', 'lionlab' ),
	);
	$args = array(
		'label' => __( 'Bolig', 'lionlab' ),
		'description' => __( '', 'lionlab' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-admin-home',
		'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'author', 'page-attributes'),
		'taxonomies' => array('category'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'bolig', $args );

}

add_action( 'init', 'create_bolig_cpt', 0 );

?>