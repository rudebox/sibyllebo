<?php
 /**
   * Description: Lionlab helpers
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

	//remove countrycode from phone
	function get_formatted_phone($str) {
	  
	  // Remove +45
	  $str = str_replace('+', '00', $str);

	  // Only allow integers
	  $str = preg_replace('/[^0-9]/s', '', $str);

	  return $str;
	}


	//allow svg uploads
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');


	// Custom excerpt length
	function custom_excerpt_length( $length ) {
	  return 20;
	}

	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


	// Custom excerpt text
	function custom_excerpt_more( $more ) {
	  return '&hellip;';
	}

	add_filter('excerpt_more', 'custom_excerpt_more');


	// Move Yoast to bottom
	function yoasttobottom() {
	  return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


	//get proper title
	function get_proper_title( $id = null  ) {
	  
	  if ( is_null($id) ) {
	    global $post;
	    $id = $post->ID;
	  }

	  $acf_title = get_field('page_title', $id);

	  return ($acf_title) ? $acf_title : get_the_title( $id );
	}


	//get primary category
	function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
	    $return = array();

	    if (class_exists('WPSEO_Primary_Term')){
	        // Show Primary category by Yoast if it is enabled & set
	        $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
	        $primary_term = get_term($wpseo_primary_term->get_primary_term());

	        if (!is_wp_error($primary_term)){
	            $return['primary_category'] = $primary_term;
	        }
	    }

	    if (empty($return['primary_category']) || $return_all_categories){
	        $categories_list = get_the_terms($post_id, $term);

	        if (empty($return['primary_category']) && !empty($categories_list)){
	            $return['primary_category'] = $categories_list[0];  //get the first category
	        }
	        if ($return_all_categories){
	            $return['all_categories'] = array();

	            if (!empty($categories_list)){
	                foreach($categories_list as &$category){
	                    $return['all_categories'][] = $category->term_id;
	                }
	            }
	        }
	    }

	    return $return;
	}

?>