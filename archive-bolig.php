<?php get_template_part('parts/header'); ?>

<main itemscope itemtype="http://schema.org/SearchResultsPage">
  <?php 
    //include template parts
    get_template_part('parts/page', 'header'); 
    get_template_part('parts/google', 'maps'); 

    //count serch results
    $allsearch = new WP_Query("s=$s&showposts=0"); 

   ?>

    <section class="appartments padding--both">
      <div class="wrap hpad">
        <div class="row flex flex--wrap">
          <?php if (have_posts() ) : while (have_posts()): the_post(); ?>

            <?php 
              //post img
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
              //post img alt tag
              $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);   

              //get primary category - see function in lionlab-helpers.php
              $post_categories = get_post_primary_category($post->ID, 'category');

              $primary_category = $post_categories['primary_category']->name;

              $squaremeter = get_field('squaremeter');
              $rooms = get_field('rooms');
              $period = get_field('rent_period');
              $date = get_field('rent_date');
              $rent = get_field('rent');
            ?>

            <a href="<?php the_permalink(); ?>" data-category="<?php echo $primary_category; ?>" title="<?php the_title_attribute(); ?>" class="col-sm-6 col-md-4 appartments__item search-result__item">
                <header>
                  <div itemprop="image" class="appartments__img" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);">
                    
                  </div>
                </header>

                <div class="appartments__content appartments__content--archive" itemprop="description">
                  <h2 class="appartments__title h5" itemprop="name">                   
                      <?php the_title(); ?>
                  </h2>
                  <div class="flex flex--justify flex--wrap appartment__details">
                    <span><span class="blue-medium">Kvm</span> <?php echo esc_html($squaremeter); ?></span> 
                    <span><span class="blue-medium">Lejeperiode</span> <?php echo esc_html($period); ?></span>
                  </div>
                  <div class="flex flex--justify flex--wrap appartments__details">
                    <span><span class="blue-medium">Rum</span> <?php echo esc_html($rooms); ?></span>            
                    <span><span class="blue-medium">Overtagelse</span> <?php echo esc_html($date); ?></span>
                  </div>
                  <div class="flex flex--justify flex--wrap appartment__details">
                    <span>Månedlig leje</span><strong><?php echo esc_html($rent); ?>,-</strong>
                  </div>
                </div>
            </a>
            <?php endwhile; else: ?>
              
            <div class="search-result__query padding--top">
              <h4><?php _e('Din søgning for', 'lionlab'); ?> <strong><?php echo esc_attr(get_search_query()); ?></strong> <?php _e('gav ingen resultater', 'lionlab'); ?></h4>
            </div>
            <?php endif; ?>
        </div>
      </div>
    </section>

</main>

<?php get_template_part('parts/footer'); ?>